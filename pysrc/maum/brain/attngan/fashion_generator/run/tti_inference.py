from __future__ import print_function

import re
import spacy
import random
import pprint
import argparse
import datetime
import dateutil.tz
import numpy as np
import torch
from torch.autograd import Variable
from spacy.tokenizer import Tokenizer
from PIL import Image
import pickle
import base64

from maum.brain.attngan.fashion_generator.core.model import RNN_ENCODER, G_NET
from maum.brain.attngan.fashion_generator.core.config import *
from maum.brain.attngan.fashion_generator.core.utils import mkdir_p


class TTI:
    def __init__(self, cfg_file, device, data_dir='', manualSeed=None):
        if cfg_file is not None:
            cfg_from_file(cfg_file)

        if device != -1:
            cfg.GPU_ID = device
        else:
            cfg.CUDA = False

        if data_dir != '':
            cfg.DATA_DIR = data_dir
        print('Using config:')
        pprint.pprint(cfg)

        if not cfg.TRAIN.FLAG:
            manualSeed = 100
        elif manualSeed is None:
            manualSeed = random.randint(1, 10000)
        random.seed(manualSeed)
        np.random.seed(manualSeed)
        torch.manual_seed(manualSeed)
        if cfg.CUDA:
            torch.cuda.manual_seed_all(manualSeed)

        nlp = spacy.load("en_core_web_md")
        self.tokenizer = Tokenizer(nlp.vocab)

        if cfg.TRAIN.NET_G == '':
            print('Error: the path for morels is not found!')
        else:
            self.text_encoder = RNN_ENCODER(None, nhidden=cfg.TEXT.EMBEDDING_DIM)
            state_dict = torch.load(cfg.TRAIN.NET_E, map_location=lambda storage, loc: storage)
            self.text_encoder.load_state_dict(state_dict)
            print('Load text encoder from:', cfg.TRAIN.NET_E)
            self.text_encoder.cuda()
            self.text_encoder.eval()
            self.caption_pkl = pickle.load(open('/home/msl/gom/brain-tti/pysrc/maum/brain/attngan/fashion_generator/core/captions1.pickle', 'rb'))
            self.netG = G_NET()
            self.s_tmp = cfg.TRAIN.NET_G[:cfg.TRAIN.NET_G.rfind('.pth')]
            state_dict = torch.load(cfg.TRAIN.NET_G, map_location=lambda storage, loc: storage)
            self.netG.load_state_dict(state_dict)
            print('Load G from: ', cfg.TRAIN.NET_G)
            self.netG.cuda()
            self.netG.eval()

    def gen_example(self, setences=None):
        '''generate images from example sentences'''
        now = datetime.datetime.now(dateutil.tz.tzlocal())
        timestamp = now.strftime('%Y_%m_%d_%H_%M_%S')

        if setences is None:
            filepath = '%sexample_filenames.txt' % (cfg.DATA_DIR)
            setences = open(filepath, "r").read().encode().decode('utf8').split('\n')
        else:
            if type(setences) == str:
                setences = setences.split('\n')

        captions_ar = []
        cap_lens = []
        for sent in setences:
            if len(sent) == 0:
                continue

            sent = re.sub("[^A-Za-z0-9-']+", ' ', sent).lower()
            toks = self.tokenizer(sent)
            tokens = [token.text for token in toks]

            if len(tokens) == 0:
                print('sent', sent)
                continue

            rev = []
            for t in tokens:
                t = t.encode('ascii', 'ignore').decode('ascii')
                if len(t) > 0 and t in self.caption_pkl[3]:
                    rev.append(self.caption_pkl[3][t])
            captions_ar.append(rev)
            cap_lens.append(len(rev))

        max_len = np.max(cap_lens)
        sorted_indices = np.argsort(cap_lens)[::-1]
        cap_lens = np.asarray(cap_lens)
        cap_lens = cap_lens[sorted_indices]
        cap_array = np.zeros((len(captions_ar), max_len), dtype='int64')

        data_dic = {}
        for i in range(len(captions_ar)):
            idx = sorted_indices[i]
            cap = captions_ar[idx]
            c_len = len(cap)
            cap_array[i, :c_len] = cap
        key = timestamp
        data_dic[key] = [cap_array, cap_lens, sorted_indices]

        for idx, key in enumerate(data_dic):
            save_dir = '%s/%s' % (self.s_tmp, key)
            mkdir_p(save_dir)
            captions, cap_lens, sorted_indices = data_dic[key]

            batch_size = captions.shape[0]
            nz = cfg.GAN.Z_DIM
            captions = Variable(torch.from_numpy(captions), volatile=True)
            cap_lens = Variable(torch.from_numpy(cap_lens), volatile=True)

            captions = captions.cuda()
            cap_lens = cap_lens.cuda()
            for i in range(1):  # 16
                noise = Variable(torch.FloatTensor(batch_size, nz), volatile=True)
                noise = noise.cuda()

                hidden = self.text_encoder.init_hidden(batch_size)

                words_embs, sent_emb = self.text_encoder(captions, cap_lens, hidden)
                mask = (captions == 0)

                noise.data.normal_(0, 1)
                fake_imgs, attention_maps, _, _ = self.netG(noise, sent_emb, words_embs, mask)

                for j in range(batch_size):
                    save_name = '%s/%d_s_%d' % (save_dir, i, sorted_indices[j])
                    im = fake_imgs[-1][j].data.cpu().numpy()
                    im_array = im.sum()

                    im = (im + 1.0) * 127.5
                    im = im.astype(np.uint8)
                    # print('im', im.shape)
                    im = np.transpose(im, (1, 2, 0))
                    # print('im', im.shape)
                    im = Image.fromarray(im)
                    fullpath = '%s_g%d.png' % (save_name, j)
                    im.save(fullpath)

        saved_img = open(fullpath, 'rb').read()
        result_output = base64.b64encode(saved_img).decode("utf-8")
        return saved_img, im_array


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train a AttnGAN network')
    parser.add_argument('--cfg', dest='cfg_file',
                        help='optional config file', type=str,
                        default='/home/msl/gom/brain-tti/pysrc/maum/brain/attngan/fashion_generator/core/eval_fashion_gen.yml')
    parser.add_argument('--gpu', dest='gpu_id', type=int, default=-1)
    parser.add_argument('--data_dir', dest='data_dir', type=str, default=cfg.DATA_DIR)
    parser.add_argument('--caption', type=str, default=None)
    args = parser.parse_args()

    tti = TTI(args.cfg_file, args.gpu_id, args.data_dir)

    print(tti.gen_example(args.caption))
