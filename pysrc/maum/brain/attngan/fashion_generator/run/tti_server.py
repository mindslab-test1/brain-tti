from concurrent import futures

import logging
import time
import grpc
import argparse
import sys, os

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))

from maum.brain.attngan.fashion_generator.run.tti_inference import TTI as TTF
from maum.brain.attngan.fashion_generator.tti_pb2 import OutputImg
from maum.brain.attngan.fashion_generator.tti_pb2_grpc import TTIServicer
from maum.brain.attngan.fashion_generator.tti_pb2_grpc import add_TTIServicer_to_server


_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class TTI(TTIServicer):
    def __init__(self, cfg_file, gpu_id, data_dir):
        self.model = TTF(cfg_file, gpu_id, data_dir)

    def GenImg(self, msg, context):
        byte_img = self.model.gen_example(msg.caption)
        output = OutputImg(byteimg=byte_img)
        return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='fashion_tti runner executor')
    print(parser.description)
    parser.add_argument('--cfg', dest='cfg_file',
                        help='optional config file', type=str,
                        default='pysrc/maum/brain/attngan/fashion_generator/core/eval_fashion_gen.yml')

    parser.add_argument('--gpu', dest='gpu'
                                      '._id', type=int, default=-1)
    parser.add_argument('--data_dir', dest='data_dir', type=str, default='')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=36001)

    args = parser.parse_args()

    tti = TTI(args.cfg_file, args.gpu_id, args.data_dir)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_TTIServicer_to_server(tti, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()
    logging.info('fashion_tti starting at 0.0.0.0:%d' % args.port)
    print('fashion_tti starting at 0.0.0.0:%d' % args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


