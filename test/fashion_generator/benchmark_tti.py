from pysrc.maum.brain.attngan.fashion_generator.run.tti_inference import TTI

"""
# config_file_path: infer_core/cfg/eval_fashion_gen.yml,
# data_dir: model_path
"""
cfg_file = 'pysrc/maum/brain/attngan/fashion_generator/core/eval_fashion_gen.yml'
gpu_id = 0
data_dir = '/maum//trained/tti/'


############### test case 1 ###############
def test_sample_text(benchmark):
    tti = TTI(cfg_file, gpu_id, data_dir)

    text = "Reversible long sleeve nylon bomber jacket in 'incense' tan. " \
           "Rib knit stand collar, cuffs, and hem. Zip closure at front. " \
           "Welt pockets at waist. Utility pocket at upper sleeve. " \
           "Reverse in pink technical satin. Silver-tone hardware. Tonal stitching."

    benchmark(tti.gen_example, text)


############### test case 2 ###############
def test_short_text(benchmark):
    tti = TTI(cfg_file, gpu_id, data_dir)

    text = "short sleeve t-shirts in black with v-nec collar."

    benchmark(tti.gen_example, text)


############### test case 3 ###############
def test_long_text(benchmark):
    tti = TTI(cfg_file, gpu_id, data_dir)

    text = "Long sleeve cotton fleece sweatshirt featuring 'woodland' camouflage pattern in tones of brown, green, and black. " \
           "Distressing throughout. " \
           "Rib knit crewneck collar, cuffs, and hem. " \
           "Graphic printed in white at front and back. " \
           "Concealed zippered welt pockets at sides. " \
           "Zippered vent featuring concealed silver-tone logo patch at cuffs. " \
           "Logo flag and signature logo ribbon at hem. " \
           "Antiqued gold-tone hard"

    benchmark(tti.gen_example, text)
