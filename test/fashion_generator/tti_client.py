from __future__ import print_function

import grpc
import argparse

import maum.brain.attngan.fashion.mrc.tti_pb2
import maum.brain.bert.mrc.tti_pb2_grpc


def genimg(stub, input_caption):
    return stub.GenImg(tti_pb2.InputText(caption=input_caption))


def run(caption, host, port):
    with grpc.insecure_channel(host+':'+str(port)) as channel:
        stub = tti_pb2_grpc.TTIStub(channel)
        return genimg(stub, caption)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--port', type=int, default=36001)
    parser.add_argument('-c', '--caption', type=str,
                        default="Slim-fit 'classic loop back' French terry lounge pants in heather grey."
                                "Drawstring at rib knit waistband. Three-pocket styling."
                                "Signature 'engineered four bar' stripes knit in white at thigh. Rib knit cuffs."
                                "Logo patch in white at cuff."
                                "Signature tricolor grosgrain pull-loop at back waistband. Tonal stitching.")
    args = parser.parse_args()
    run(args.caption, args.host, args.port)
    print('Done')
